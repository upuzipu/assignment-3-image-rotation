#include <stdio.h>
#include <stdlib.h>

#include "image_bmp.h"
#include "image_transform.h"
#include "io.h"

int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "Usage: %s <source> <destination>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  char* source_fname = argv[1];
  char* destination_fname = argv[2];

  file_byte_reader source_reader = file_byte_reader_open(source_fname);

  struct image source;

  switch (image_from_bmp(&source, source_reader)) {
    case READ_OK: {
      break;
    }
    case READ_INVALID_HEADER: {
      fprintf(stderr, "Error: %s is not a valid image\n", source_fname);
      exit(EXIT_FAILURE);
    }
    case READ_INVALID_SIGNATURE: {
      fprintf(stderr, "Error: format is not supported\n");
      exit(EXIT_FAILURE);
    }
    case READ_INVALID_BITS: {
      fprintf(stderr, "Error: %s is corrupted\n", source_fname);
      exit(EXIT_FAILURE);
    }
    default: {
      fprintf(stderr, "Error: couldn't read image from %s\n", source_fname);
      exit(EXIT_FAILURE);
    }
  }

  file_byte_reader_close(&source_reader);

  struct image destination = image_rotate_counterclockwise(source);

  image_destroy(&source);

  file_byte_writer destination_writer = file_byte_writer_open(destination_fname);

  switch (image_to_bmp(&destination, destination_writer)) {
    case WRITE_OK: {
      break;
    }
    default: {
      fprintf(stderr, "Error: couldn't save image to %s\n", destination_fname);
	  exit(EXIT_FAILURE);
    }
  }

  image_destroy(&destination);

  return 0;
}
