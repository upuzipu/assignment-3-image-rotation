#include "io.h"

#include <stdint.h>

file_byte_reader file_byte_reader_open(const char* restrict filename) {
  return (file_byte_reader) { fopen(filename, "rb") };
}

file_byte_writer file_byte_writer_open(const char* restrict filename) {
  return (file_byte_writer) { fopen(filename, "wb") };
}

void file_byte_reader_close(file_byte_reader* restrict reader) {
  fclose(reader->f);
  reader->f = NULL;
}

void file_byte_writer_close(file_byte_writer* restrict writer) {
  fclose(writer->f);
  writer->f = NULL;
}

enum read_status file_byte_reader_read(
  file_byte_reader reader, void* buffer, size_t size, size_t elements, size_t padding
) {
  size_t read_count = fread(buffer, size, elements, reader.f);
  if (read_count != elements) {
    return READ_ERROR;
  }

  fseek(reader.f, (long) padding, SEEK_CUR);
  return READ_OK;
}

enum write_status file_byte_writer_write(
  file_byte_writer writer, void* buffer, size_t size, size_t elements, size_t padding
) {
  size_t write_count = fwrite(buffer, size, elements, writer.f);
  if (write_count != elements) {
    return WRITE_ERROR;
  }

  uint8_t zeros[3] = { 0 };
  fwrite(zeros, padding, 1, writer.f);

  return WRITE_OK;
}

bool file_byte_reader_jump(file_byte_reader reader, size_t where) {
  return fseek(reader.f, (long) where, SEEK_SET) == 0;
}

bool file_byte_writer_jump(file_byte_writer writer, size_t where) {
  return fseek(writer.f, (long) where, SEEK_SET) == 0;
}
