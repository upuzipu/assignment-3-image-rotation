#include "image.h"

struct image image_create(size_t width, size_t height) {
  return (struct image) {
    .width = width,
    .height = height,
    .data = malloc(width * height * sizeof(struct pixel))
  };
}

void image_destroy(struct image* restrict image) {
  free(image->data);
}

const struct pixel* image_get(const struct image* restrict image, size_t x, size_t y) {
  return &image->data[x + y * image->width];
}

void image_set(const struct image* restrict image, size_t x, size_t y, const struct pixel* color) {
  image->data[x + y * image->width] = *color;
}
