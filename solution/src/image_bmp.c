#include "image_bmp.h"

#include <inttypes.h>
#include <limits.h>
#include <stdint.h>
#include <string.h>

struct __attribute__((packed)) bmp_file_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
};

struct __attribute__((packed)) bmp_info_header {
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

enum {
  BI_RGB = 0,
  BI_PELS_PER_METER = 0,
  BI_USED_ALL = 0,
  BI_IMPORTANT_ALL = 0
};

struct __attribute__((packed)) bmp_header {
  struct bmp_file_header bf;
  struct bmp_info_header bi;
};

static inline size_t padded_row_size(size_t row_size) {
  return ((row_size + 3) / 4) * 4;
}

static inline size_t padding_size(size_t row_size) {
  return padded_row_size(row_size) - row_size;
}

union bmp_file_type {
  char as_string[3];
  struct {
    uint16_t as_bfType;
    uint8_t _nt; // null terminator
  };
};

static enum read_status bmp_header_read(struct bmp_header* restrict header, file_byte_reader reader) {
  if (file_byte_reader_read(reader, &header->bf, sizeof(struct bmp_file_header), 1, 0) != READ_OK) {
    return READ_INVALID_HEADER;
  }

  union bmp_file_type file_type = (union bmp_file_type) {
    .as_bfType = header->bf.bfType, 0
  };

  if (strcmp(file_type.as_string, "BM") != 0) {
    return READ_INVALID_SIGNATURE;
  }

  if (file_byte_reader_read(reader, &header->bi, sizeof(struct bmp_info_header), 1, 0) != READ_OK) {
    return READ_INVALID_HEADER;
  }

  file_byte_reader_jump(reader, header->bf.bOffBits);

  return READ_OK;
}

enum read_status image_from_bmp(struct image* restrict image, file_byte_reader reader) {
  struct bmp_header header;
  enum read_status header_read_status = bmp_header_read(&header, reader);

  if (header_read_status != READ_OK) {
    return header_read_status;
  }

  *image = image_create(header.bi.biWidth, header.bi.biHeight);
  
  size_t padding = padding_size(image->width * sizeof(struct pixel));

  for (size_t i = 0; i < image->height; ++i) {
    struct pixel* row = image->data + (i * image->width);
    if (file_byte_reader_read(reader, row, sizeof(struct pixel), image->width, padding) != READ_OK) {
      return READ_INVALID_BITS;
    }
  }

  return READ_OK;
}

struct bmp_header bmp_header_for(const struct image* restrict image) {
  union bmp_file_type file_type = { .as_string = "BM" };
  uint16_t bfType = file_type.as_bfType;
  uint32_t bOffBits = sizeof(struct bmp_header);
  uint32_t bfReserved = 0;
  uint32_t biSizeImage = (padded_row_size(image->width * sizeof(struct pixel)) * image->height);

  return (struct bmp_header) {
    .bf = {
      .bfType = bfType,
      .bfileSize = bOffBits + biSizeImage,
      .bfReserved = bfReserved,
      .bOffBits = bOffBits
    },
    .bi = {
      .biSize = sizeof(struct bmp_info_header),
      .biWidth = image->width,
      .biHeight = image->height,
      .biPlanes = 1,
      .biBitCount = sizeof(struct pixel) * CHAR_BIT,
      .biCompression = BI_RGB,
      .biSizeImage = biSizeImage,
      .biXPelsPerMeter = BI_PELS_PER_METER,
      .biYPelsPerMeter = BI_PELS_PER_METER,
      .biClrUsed = BI_USED_ALL,
      .biClrImportant = BI_IMPORTANT_ALL
    }
  };
}

enum write_status image_to_bmp(const struct image* restrict image, file_byte_writer writer) {
  struct bmp_header header = bmp_header_for(image);
  if (file_byte_writer_write(writer, &header, sizeof(struct bmp_header), 1, 0) != WRITE_OK) {
    return WRITE_ERROR;
  }

  size_t padding = padding_size(image->width * sizeof(struct pixel));

  for (size_t i = 0; i < image->height; ++i) {
    struct pixel* row = image->data + (i * image->width);

    if (file_byte_writer_write(writer, row, sizeof(struct pixel), image->width, padding) != WRITE_OK) {
      return WRITE_ERROR;
    }
  }

  return WRITE_OK;
}
