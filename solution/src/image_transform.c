#include "image_transform.h"

struct image image_rotate_counterclockwise(struct image image) {
	struct image rotated_image = image_create(image.height, image.width);

	for (size_t y = 0; y < image.height; ++y) {
		for (size_t x = 0; x < image.width; ++x) {
			const struct pixel* source_color = image_get(&image, x, y);
			image_set(&rotated_image, image.height - y - 1, x, source_color);
		}
	}

	return rotated_image;
}
