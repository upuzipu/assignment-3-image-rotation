#ifndef _IO_H
#define _IO_H

#include <stdbool.h>
#include <stdio.h>

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
};

enum write_status {
  WRITE_OK = 0,
  WRITE_ERROR
};

typedef struct {
  FILE* f;
} file_byte_reader;

typedef struct {
  FILE* f;
} file_byte_writer;

file_byte_reader file_byte_reader_open(const char* restrict filename);
file_byte_writer file_byte_writer_open(const char* restrict filename);

void file_byte_reader_close(file_byte_reader* restrict reader);
void file_byte_writer_close(file_byte_writer* restrict writer);

enum read_status file_byte_reader_read(
  file_byte_reader reader, void* buffer, size_t size, size_t elements, size_t padding
);

enum write_status file_byte_writer_write(
  file_byte_writer writer, void* buffer, size_t size, size_t elements, size_t padding
);

bool file_byte_reader_jump(file_byte_reader reader, size_t where);
bool file_byte_writer_jump(file_byte_writer writer, size_t where);

#endif
