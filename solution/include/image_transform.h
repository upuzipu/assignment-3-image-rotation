#ifndef _TRANSFORM_H
#define _TRANSFORM_H

#include "image.h"

struct image image_rotate_counterclockwise(struct image image);

#endif
