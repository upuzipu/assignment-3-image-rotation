#ifndef _IMAGE_H
#define _IMAGE_H

#include <stdint.h>
#include <stdlib.h>

#define PIXEL_SIZE sizeof(struct pixel)

struct __attribute__((packed)) pixel {
  uint8_t b;
  uint8_t g;
  uint8_t r;
};

struct image {
  size_t width;
  size_t height;
  struct pixel* data;
};

struct image image_create(size_t width, size_t height);
void image_destroy(struct image* restrict image);

const struct pixel* image_get(const struct image* restrict image, size_t x, size_t y);
void image_set(const struct image* restrict image, size_t x, size_t y, const struct pixel* color);

#endif
