#ifndef _IMAGE_BMP_H
#define _IMAGE_BMP_H

#include <stdio.h>

#include "image.h"
#include "io.h"

enum read_status image_from_bmp(struct image* restrict image, file_byte_reader reader);
enum write_status image_to_bmp(const struct image* restrict image, file_byte_writer writer);

#endif
